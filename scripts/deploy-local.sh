#!/bin/sh
projectRoot=$(pwd)

pm2 stop web-client
pm2 stop local-server
pm2 delete web-client
pm2 delete local-server

rm -rf ./dist
mkdir dist

# Build the web client
cd ./packages/web-client
npm run build

mkdir "${projectRoot}/dist/web-client"
cp -R ./build/* "${projectRoot}/dist/web-client"

# Build the local server
cd "${projectRoot}"
cd ./packages/local-server
npm run build

mkdir "${projectRoot}/dist/local-server"
cp -R ./dist/* "${projectRoot}/dist/local-server"
cp -R ./.env.development "/Users/niftyone/Work/locco/dist/local-server"
mkdir -p "${projectRoot}/dist/local-server/node_modules" && npm ls --only prod --parseable | tail -n +2 | xargs -I % cp -r % "${projectRoot}/dist/local-server/node_modules"

# Add necesasry pacakges
cd "${projectRoot}/dist"

echo "module.exports = {
    apps: [{
            name: 'web-client',
            script: 'http-server ./web-client -p 3200 -a 0.0.0.0',
        },
        {
            name: 'local-server',
            script: 'node ./local-server/bootstrap/start.js',
            env: {
                HOST: \"0.0.0.0\",
                PORT: \"3100\"
            },
        },
    ],
};" > pm2.config.js

pm2 start pm2.config.js
pm2 save