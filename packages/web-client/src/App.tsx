import React from 'react';
import { AppProvider } from '@shopify/polaris';
import enTranslations from '@shopify/polaris/locales/en.json';

import '@shopify/polaris/styles.css';

import ControlsPage from './pages/ControlsPage';

const App = () => (
  <AppProvider i18n={enTranslations}>
    <ControlsPage />
  </AppProvider>
);

export default App;
