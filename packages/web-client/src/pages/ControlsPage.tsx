import React from 'react';
import { Provider } from 'mobx-react';
import { Page } from '@shopify/polaris';

import ControlsStore from '../stores/controls.store';
import SystemControls from '../components/SystemControls';
import MediaControls from '../components/MediaControls';

class ControlsPage extends React.Component {
  private controlsStore: ControlsStore = new ControlsStore();

  render() {
    return (
      <Provider controlsStore={this.controlsStore}>
        <Page>
          <SystemControls />
          <MediaControls />
        </Page>
      </Provider>
    );
  }
}
export default ControlsPage;
