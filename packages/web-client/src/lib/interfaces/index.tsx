export type Stage = 'production' | 'development';

export type LogLevel = 'fatal' | 'error' | 'warn' | 'info' | 'debug' | 'trace';

export interface ILogConfig {
  level: LogLevel;
}

export interface IAPIConfig {
  host: string;
  path: string;
}
export interface IConfig {
  name: string;
  stage: Stage;
  host: string;
  api: IAPIConfig;
  log: ILogConfig;
}

export interface IEnv {
  stage: Stage;
  host: string;
  apiHost: string;
  apiPath: string;
  logLevel: LogLevel;
}

export type MediaType = 'netflix' | 'youtube' | null;
