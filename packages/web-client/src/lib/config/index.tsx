import { IConfig, IEnv, Stage, IAPIConfig, LogLevel, ILogConfig } from '../interfaces';
// import * as localPackage from '../../../package.json';
// import * as mainPackage from '../../../../../package.json';

function getEnv(): IEnv {
  return {
    host: process.env.REACT_APP_HOST || '',
    stage: (process.env.REACT_APP_STAGE as Stage) || 'development',
    apiHost: process.env.REACT_APP_API_HOST || '',
    apiPath: process.env.REACT_APP_API_PATH || '',
    logLevel: process.env.REACT_APP_LOG_LEVEL as LogLevel,
  };
}

export function getConfig(_stage?: Stage): IConfig {
  const env = getEnv();

  const name = `${'asd'}/${'asasdd2'}`;

  const api: IAPIConfig = {
    host: env.apiHost,
    path: env.apiPath,
  };

  const log: ILogConfig = {
    level: env.logLevel,
  };

  return {
    name,
    stage: env.stage,
    host: env.host,
    api,
    log,
  };
}

const config = getConfig();

export default config;
