import pino from 'pino';
import config from './config';

export const baseLogger = pino({
  name: config.name,
  level: config.log.level,
  browser: {
    write: ({ time, level, stage, ...payload }: any) => {
      console.log(`[${pino.levels.labels[level].toUpperCase()}][${stage.toUpperCase()}]`, payload);
    },
  },
  prettyPrint: true,
});

const logger = baseLogger.child({ stage: config.stage });

export default logger;
