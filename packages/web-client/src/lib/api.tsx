import _ from 'lodash';
import config from './config';
import { IAPIConfig } from './interfaces';

export class API {
  protected basePath: string;

  constructor(props: IAPIConfig) {
    this.basePath = _.trim(`http://${props.host}`, '/');
    if (props.path) this.basePath = _.trim(`${this.basePath}/${props.path}`, '/');
  }

  async systemCommandSleep(minutes: number): Promise<boolean> {
    await fetch(`${this.basePath}/command/system/sleep?minutes=${minutes}`);
    return true;
  }

  async systemCommandVolumeUp(): Promise<boolean> {
    await fetch(`${this.basePath}/command/system/volume-up`);
    return true;
  }

  async systemCommandVolumeDown(): Promise<boolean> {
    await fetch(`${this.basePath}/command/system/volume-down`);
    return true;
  }
}

const api = new API(config.api);
export default api;
