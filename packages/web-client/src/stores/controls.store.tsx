import { observable, action } from 'mobx';
import { MediaType } from '../lib/interfaces';

import API from '../lib/api';

export default class ControlsStore {
  @observable
  mediaType: MediaType = null;

  @observable
  requestRunning: boolean = false;

  @action
  fetchMediaType() {
    console.log(this.mediaType);

    this.mediaType = this.mediaType === 'netflix' ? 'youtube' : 'netflix';
  }

  @action
  async systemCommandSleep(minutes: number) {
    this.requestRunning = true;
    await API.systemCommandSleep(minutes);
    this.requestRunning = false;
  }

  @action
  async systemCommandVolumeUp() {
    await API.systemCommandVolumeUp();
  }

  @action
  async systemCommandVolumeDown() {
    await API.systemCommandVolumeDown();
  }
}

export const controlsStore = new ControlsStore();
