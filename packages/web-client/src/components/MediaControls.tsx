import React from 'react';
import { Card, Button, ButtonGroup } from '@shopify/polaris';
import { observer, inject } from 'mobx-react';

import ControlsStore from '../stores/controls.store';

@inject('controlsStore')
@observer
class MediaControls extends React.Component {
  private controlsStore: ControlsStore = new ControlsStore();

  render() {
    return (
      <Card title="Media" sectioned>
        <ButtonGroup>
          <Button onClick={() => alert('Button clicked!')}>Play/Pause</Button>
          <Button onClick={() => alert('Button clicked!')}>Seek Back</Button>
          <Button onClick={() => alert('Button clicked!')}>Seek Fwd</Button>
        </ButtonGroup>
      </Card>
    );
  }
}

export default MediaControls;
