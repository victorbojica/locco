import React from 'react';
import { Card, Button, ButtonGroup } from '@shopify/polaris';
import { observer, inject } from 'mobx-react';

import ControlsStore from '../stores/controls.store';
import SleepTimerButton from './SleepTimerButton';

@inject('controlsStore')
@observer
class SystemControls extends React.Component {
  private controlsStore: ControlsStore = new ControlsStore();

  render() {
    return (
      <Card title="System" sectioned>
        <ButtonGroup>
          <SleepTimerButton onSubmit={(minutes) => this.controlsStore.systemCommandSleep(minutes)}>
            Sleep
          </SleepTimerButton>
          <Button onClick={() => this.controlsStore.systemCommandVolumeUp()}>Volume Up</Button>
          <Button onClick={() => this.controlsStore.systemCommandVolumeDown()}>Volume Down</Button>
        </ButtonGroup>
      </Card>
    );
  }
}

export default SystemControls;
