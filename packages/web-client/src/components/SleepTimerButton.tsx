import React, { useState, useCallback } from 'react';
import { Button, Modal, TextContainer, TextField } from '@shopify/polaris';
import logger from '../lib/logger';

interface SleepTimerButtonProps {
  children?: string | string[];
  onSubmit(minutes: number): void;
}

const SleepTimerButton = (props: SleepTimerButtonProps) => {
  const [active, setActive] = useState(false);
  const [minutes, setMinutes] = useState(0);

  const toggleModal = useCallback(() => setActive(!active), [active]);

  return (
    <>
      <Button onClick={toggleModal}>{props.children}</Button>
      <Modal
        open={active}
        onClose={toggleModal}
        title="Timer"
        primaryAction={{
          content: 'Submit',
          onAction: () => {
            props.onSubmit(minutes);
            toggleModal();
          },
        }}
      >
        <Modal.Section>
          <TextContainer>
            <TextField
              label="Minutes"
              type="number"
              value={minutes.toString()}
              onChange={(value) => {
                logger.info({ value });
                setMinutes(parseInt(value));
              }}
            />
          </TextContainer>
        </Modal.Section>
      </Modal>
    </>
  );
};

export default SleepTimerButton;
