import { v4 as uuid } from 'uuid';
import { Request, Response, NextFunction } from 'express';

import baseLogger from '../../lib/logger';

const getRequestLogProps = (req: Request) => {
  return {
    correlationId: req.context.metadata.correlationId,
    context: {
      req_url: `${req.method} ${req.originalUrl}`,
      method: req.method,
      url: req.originalUrl,
      headers: req.headers,
      query: req.query,
      params: req.params,
      body: req.body,
    },
  };
};

const middleware = () => {
  const injectLogger = (req: Request, res: Response, next: NextFunction) => {
    const logger = baseLogger.child(getRequestLogProps(req));

    req.context = {
      ...req.context,
      logger,
    };

    next();
  };

  const requestStart = (req: Request, res: Response, next: NextFunction) => {
    const logger = req.context.logger.child({
      startDate: req.context.metadata.startDate.toDateString(),
    });

    logger.info('REQ :: START');

    next();
  };

  const requestEnd = (req: Request, res: Response, next: NextFunction) => {
    req.on('end', () => {
      const logger = req.context.logger.child({
        duration: req.context.metadata.duration,
      });

      logger.info('REQ :: END');
    });

    next();
  };

  return [injectLogger, requestStart, requestEnd];
};

export default middleware;
