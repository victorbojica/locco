import { v4 as uuid } from 'uuid';
import { Request, Response, NextFunction } from 'express';

const middleware = () => (req: Request, res: Response, next: NextFunction) => {
  let id = uuid();
  // see if we have a correlation_id from the req
  let correlationId = req.header('correlation-id') || id;
  let startDate = new Date();

  const metadata = {
    startDate,
    correlationId,
  };

  // add the req/res correlation id
  const context = {
    metadata,
    correlationId,
  };

  req.context = context;

  req.headers.correlationId = correlationId;
  res.header('correlation-id', correlationId);

  req.on('end', () => {
    const endDate = new Date();
    const { startDate } = req.context.metadata;

    const duration = endDate.getTime() - startDate.getTime();

    req.context.metadata = {
      ...req.context.metadata,
      endDate,
      duration,
    };
  });

  next();
};

export default middleware;
