import "reflect-metadata";

import { Server } from 'http';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import helmet from 'helmet';
import { v4 as uuid } from 'uuid';

import config from '../lib/config';
import logger from '../lib/logger';

import { RegisterRoutes } from './routes';
import loggerMiddleware from './middleware/logger';
import metadataMiddleware from './middleware/metadata';

import '../controllers/SystemCommand.controller';

export const server = () => {
  const app = express();

  app.use(metadataMiddleware());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.use(cors());
  app.use(helmet());
  app.use(loggerMiddleware());
  app.use((_req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', `Origin, X-Requested-With, Content-Type, Accept, Authorization`);
    next();
  });

  RegisterRoutes(app);

  interface IError {
    status?: number;
    fields?: string[];
    message?: string;
    name?: string;
  }

  app.use((err: IError, _req: express.Request, res: express.Response, next: express.NextFunction) => {
    const status = err.status || 500;
    const body = {
      fields: err.fields || undefined,
      message: err.message || 'An error occurred during the request.',
      name: err.name,
      status,
    };
    res.status(status).json(body);
    next();
  });

  return new Promise<Server>((resolve) => {
    const s = app.listen(config.port, config.host, () => {
      logger.info(`APP :: STARTED :: Listening at http://${config.host}:${config.port}`);
      resolve(s);
    });
  });
};
