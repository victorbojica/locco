import { Singleton } from 'typescript-ioc';
import { promisify } from 'util';
import { exec as _exec } from 'child_process';
const exec = promisify(_exec);

@Singleton
export class SystemCommandService {
  protected sleepTimerTimeout = null; // this should be fine in a singleton, since it's about a single computer

  public async volumeDown(): Promise<void> {
    const currentVolume = await this.getCurrentVolume();
    const nextVolume = Math.ceil(currentVolume) - 5;
    await this.setCurrentVolume(nextVolume);
  }

  public async volumeUp(): Promise<void> {
    const currentVolume = await this.getCurrentVolume();
    const nextVolume = Math.ceil(currentVolume) + 5;
    await this.setCurrentVolume(nextVolume);
  }

  public sleepTimer(minutes: number): void {
    clearTimeout(this.sleepTimerTimeout);
    const timeout = minutes * 60 * 1000;
    setTimeout(async () => {
      await this.sleep();
    }, timeout);
  }

  protected async sleep(): Promise<void> {
    await exec('pmset sleepnow');
  }

  protected async getCurrentVolume(): Promise<number> {
    const { stdout: currentVolume } = await exec("osascript -e 'output volume of (get volume settings)'");
    return parseInt(currentVolume, 10);
  }

  protected async setCurrentVolume(volume: number): Promise<boolean> {
    if (volume < 0 || volume > 100) return false;
    await exec(`osascript -e 'set volume output volume ${volume}'`);
    return true;
  }

  protected async getLocalAddress(): Promise<string> {
    const { stdout } = await exec('ipconfig getifaddr en0');

    return stdout;
  }
}
