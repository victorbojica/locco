import { IRequestContext } from '../../interfaces';

declare module 'express' {
  export interface Request {
    context: IRequestContext;
  }
}
