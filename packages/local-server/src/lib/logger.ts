import pino from 'pino';
import config from './config';

export const baseLogger = pino({
  name: config.name,
  level: 'debug',
});

const logger = baseLogger.child({ stage: config.stage });

export default logger;
