import { Logger } from 'pino';

export type Stage = 'production' | 'development';

export interface IConfig {
  stage: Stage;
  name: string;
  host: string;
  port: number;
}

export interface IEnv {
  host: string;
  port: string;
}

export interface IRequestMetadata {
  correlationId?: string;
  startDate?: Date;
  endDate?: Date;
  duration?: number;
}

export interface IRequestContext {
  metadata: IRequestMetadata;
  logger?: Logger;
}
