import { IConfig, IEnv, Stage } from '../interfaces';
// import * as localPackage from '../../../package.json';
// import * as mainPackage from '../../../../../package.json';
import * as dotenv from 'dotenv';

function getEnv(stage: Stage): IEnv {
  let env = dotenv.config({ path: `./.env.${stage}` });

  return {
    ...env.parsed,
    host: process.env.HOST,
    port: process.env.PORT,
  };
}

export function getConfig(_stage?: Stage): IConfig {
  const stage = _stage || process.env.NODE_ENV === 'production' ? 'production' : 'development';
  const env = getEnv(stage);

  const name = `${'asd'}/${'asasdd'}`;
  const port = parseInt(env.port, 10);

  return {
    name,
    stage,
    ...env,
    port,
  };
}

const config = getConfig();

export default config;
