import { Get, Route, Query } from 'tsoa';
import { Inject } from 'typescript-ioc';
import { SystemCommandService } from '../services/SystemCommand.service';

@Route('command/system')
export class SystemCommandController {
  @Inject
  private systemCommandService: SystemCommandService;

  @Get('sleep')
  public async sleep(@Query() minutes: number = 0): Promise<boolean> {
    this.systemCommandService.sleepTimer(minutes);
    return true;
  }

  @Get('volume-up')
  public async volumeUp(): Promise<boolean> {
    await this.systemCommandService.volumeUp();
    return true;
  }

  @Get('volume-down')
  public async volumeDown(): Promise<boolean> {
    await this.systemCommandService.volumeDown();
    return true;
  }
}
